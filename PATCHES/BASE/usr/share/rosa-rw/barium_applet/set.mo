#!/bin/bash
prefix=$(echo $LANG |cut -f1 -d _)
mkdir -p ../../locale/$prefix/LC_MESSAGES/
rm ../../locale/$prefix/LC_MESSAGES/barium_applet.mo -f
msgfmt -v ./barium_applet.po -o ../../locale/$prefix/LC_MESSAGES/barium_applet.mo

TEXTDOMAIN=barium_applet
TEXTDOMAINDIR=$(realpath ../../locale)
echo 'try to translate "min"'
a=$(gettext -s "min")
echo $a
