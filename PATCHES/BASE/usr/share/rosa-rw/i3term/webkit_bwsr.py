#!/usr/bin/env python3
import sys, gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit', '3.0')
from gi.repository import Gtk,WebKit

view = WebKit.WebView()
sw = Gtk.ScrolledWindow()
sw.add(view)
win = Gtk.Window()
win.set_size_request(800, 600)
win.connect("destroy", Gtk.main_quit)
win.set_title("Barium help")
win.add(sw)
win.show_all()
view.load_uri(sys.argv[1])
Gtk.main()
