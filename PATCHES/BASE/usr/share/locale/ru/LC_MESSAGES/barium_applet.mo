��             +         �     �     �     �     �  
   �               )     6     ;     I     Z  	   i     s     w     z     �     �     �     �  6   �     �       	   #     -     6     :     @  &   ^     �     �  �  �     )  4   ,     a       %   �  A   �  '   �  %   %  
   K  (   V  "     )   �  '   �     �     �  8         9  %   O  $   u  ;   �  �   �  $   w	  Q   �	     �	     �	     
     
     5
  Q   S
     �
     �
                                                                                                                        
          	               * Applet update interval Autosave Check updates DISK USAGE Delete backup modules Disable saves Enable saves Exit HOME DIR SIZE HOME MODULE SIZE Known machines LAST SAVE MEM Mb Not a backup ROOTFS SAVE TIMEOUT Save home now Switch to backup System was switched to boot from backup. Please reboot Unknown error, sorry add machine to list of known available disabled min never remove all machines from list remove this machine from list of known sec used Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-11-15 19:03+0300
Last-Translator: Default user <a.betkher@rosalinux.ru>
Language-Team: Russian <gnu@d07.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
  * Интервал обновления апплета Авто сохранения Обновить ОС Использование диска Удалить резервные копии сохранений Отключить сохранение Включить сохранение Выход Размер домашней папки Размер модуля home.xzm Доверенные компьютеры Последнее сохранение ОЗУ Мб Загружено не с резервной копии Корневая ФС Интервал сохранений Сохранить home сейчас Переключение на резервную копию Система переключена на загрузку с резервной копии. Пожалуйста перезагрузите компьютер Не известная ошибка добавить этот компьютер в список доверенных доступно отключено мин не сохранялось очистить список удалить этот компьютер из списка доверенных сек использовано 