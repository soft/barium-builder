#!/bin/sh

#urpmi qemu qemu-img 

if [ "$1" == '--install' ]  ; then
    if [ -f ./ROSA.qcow2 ] ; then
	echo "ROSA.qcow2 already exists"
	exit
    fi
    qemu-img create -f qcow2 ROSA.qcow2 10G	
    qemu-system-x86_64 \
    -enable-kvm \
    -boot d \
    -cdrom $2 \
    -name "ROSA-INSTALLATOR" \
    -smp 2 \
    -m 2G \
    -vga std \
    -hda ROSA.qcow2
    exit
elif [ "$1" == '--start' ]  ; then   
	IMG=$2
	if echo $IMG |grep '^/dev/.*' ; then
		echo "$IMG - block device"
		qemu-system-x86_64 \
		-name "Boot from $IMG" \
		-boot c \
		-drive file=$IMG,cache=none  \
		-m 4G \
		-smp 2 \
		-vga std \
		-enable-kvm
		#-usb \
		#-device usb-host,hostbus=003,hostport=005 \
    elif  qemu-img info $IMG | grep -q "file.*raw" ; then   
		echo "$IMG - virtual machine image"
		qemu-system-x86_64 \
		-boot c \
		-enable-kvm \
		-name "ROSA" \
		-smp 2 \
		-m 3G \
		-vga std \
		-rtc base=localtime \
		-virtfs 'local,path=./,mount_tag=host0,security_model=mapped,id=host0' \
		-hda $IMG
    else
		echo "$IMG - iso"
		qemu-system-x86_64 \
		-enable-kvm \
		-boot d \
		-cdrom $IMG \
		-name "Boot from $IMG" \
		-smp 2 \
		-m 2G \
		-vga std
    fi
    exit
fi 

echo "$1 - unknown parameter"
 
