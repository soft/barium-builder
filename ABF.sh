#!/bin/bash
#set -x
#set -e
#set -u
#set +f
INIT_DIR=$(pwd)

# ABF searches for results here
RESULTS_DIR="${RESULTS_DIR:-/home/vagrant/results}"
mkdir -p $RESULTS_DIR

# aka https://gituser:password@abf.io/gituser/project.git
EXTGIT="${EXTGIT:-none}"

# ABF.cfg disable chaching
EXTCFG="${EXTCFG:-ABF}"

# Number of iso build.	Example: BUILD_ID=alpha_$BUILD_ID
BUILD_ID="${BUILD_ID:-FREE}"

# Set to 0 to save temporary directories for debugging
CLEANUP="${CLEANUP:-yes}"

# run livecd building under strace for debugging
STRACE="${STRACE:-no}"

# wait before starting the build time in sec, or zero to disable
SLEEP="${SLEEP:-0}"

# Setup tmpfs, size in Gb, or zero
TMPFS="${TMPFS:-0}"

#### functions
mktmpfs(){
    size=$1 ; dir=$2
    mkdir -p $dir
    mount -t tmpfs -o size=${size}G tmpfs $dir
}

rootfs_build(){
    dnf install -y \
	--releasever 2021.1 \
	--installroot "$1" \
	--forcearch=x86_64 \
	--nogpgcheck \
	--repofrompath betcher_,http://abf-downloads.rosalinux.ru/betcher__personal/repository/rosa2021.1/x86_64/main/release/ \
	$rootfspacks
}

clean_all(){
    . $INIT_DIR/WORKDIR/functions
    umount_all
    umount $INIT_DIR/WORKDIR/tmp 2>/dev/null
    [ $CLEANUP == 'yes' ] && rm -rf $INIT_DIR/WORKDIR/tmp 
}

#### /functions
trap 'if [ "$CLEANUP" = 1 ]; then clean_all; fi' 2 3


# fix path
PATH=${PATH}:/usr/sbin:/sbin
export PATH

STRACE_CMD=""
if [ "$STRACE" = 1 ]; then
    deps="$deps strace"
    STRACE_CMD="strace -f -r -o ${RESULTS_DIR}/strace_${BUILD_ID}.log"
fi

sleep "$SLEEP"

if [ "$DISTROSYNC" == 'yes' ]; then
    dnf --refresh -y distro-sync
fi

# host packages
deps="
coreutils git-core grep sed
tar squashfs-tools 
shc python3-markdown"

# base rootfs
rootfspacks='
dnf rpm basesystem-mandatory
rosa-repos'


if ! rpm -q ${deps} ; then
    dnf --refresh -y install ${deps}
fi

if [ "$EXTGIT" != 'none' ] ; then
    pushd /tmp
	EXTGITDIR=$(basename $EXTGIT .git)
	rm -rf $EXTGITDIR
	git clone $EXTGIT
	cp  $EXTGITDIR/* ${INIT_DIR}/ -fr
    popd
fi
source SETTINGS/BASE.cfg
[ $EXTCFG != 'none' ] && source SETTINGS/${EXTCFG}.cfg
echo "Result file: ${RESULTS_DIR}/${DISTRNAME}-${BUILD_ID}.tar.gz"

if [ $TMPFS != 0 ] ; then
    mktmpfs $TMPFS WORKDIR/tmp
else
    mkdir -p ./WORKDIR/tmp
fi

mkdir WORKDIR/$ROOTFS1
rootfs_build $(realpath WORKDIR/$ROOTFS1)
$STRACE_CMD ./build_DISTR --buildcfg  $EXTCFG
mv *.iso "${RESULTS_DIR}/$DISTRNAME-${BUILD_ID}.iso"
find / -maxdepth 2 -type f  -name '*.log' |while read a ; do cp $a ${RESULT_DIR}/ ;  done
pushd $OUTDIR
tar -czpf distro.tar.gz *
mv distro.tar.gz "${RESULTS_DIR}/$DISTRNAME-${BUILD_ID}.tar.gz"
popd

clean_all
