#!/bin/bash
mkdir /BUILD_DIR 2>/dev/null

export STAGE=$1
export LOG=/BUILD_DIR/$STAGE.log
[ "$2" ] && export PRESTAGE=$2
[ "$3" ] && export BUILDCFG=$3

for a in $(ls -1 /chroot_patches/*.sh| sort -n) ; do
    echo "Run: $a" 
    echo "Run: $a" >> $LOG 
    chmod +x $a
    $a 
done
rm -rf  /chroot_patches

