#!/bin/bash
. /chroot_patches/functions

action "Setting up dnf"
	setup-dnf || echo_exit_chroot "$action - ERROR" ${LINENO}

action "Remove packages"
	cat  /chroot_patches/rootfs.del | while read str ; do 
		echo "$str" |grep -q [[:alnum:]] || continue
		echo "$str" |grep -q '[[:space:]]*#' && continue
		dnf remove -y -q "$str"
	done

action "Orphans remove"
	dnf autoremove -y || echo_exit_chroot "$action - ERROR" ${LINENO}

action "Setting up kernel"
    rpm -qa |grep -q task-kernel ||  dnf install -y task-kernel
    if [ $(ls -1 /lib/modules |grep -v flow-abi |wc -l) -gt 1 ] ; then 
	dnf repoquery --installonly
    fi

rpm -qa > /BUILD_DIR/${STAGE}_pre.rpmqa

action "Distro-sync packages"
    dnf distro-sync -y  || echo_exit_chroot "$action - ERROR" ${LINENO}

action "Install packages"
	> /BUILD_DIR/NOTINSTALLED
        bcfg_add=''
        [ -f /chroot_patches/${BUILDCFG}.add ] && bcfg_add=/chroot_patches/${BUILDCFG}.add
	cat /chroot_patches/rootfs.add $bcfg_add |while read a  ; do
		if echo $a |egrep -q '^[[:space:]]*\#' ; then
		    echo "Commented: $a"
		    continue
		fi
		echo "Processing: $a"
		dnf install "$a" -b -y || echo $a >> /BUILD_DIR/NOTINSTALLED
	done
	
action "DNF autoremove"
    dnf autoremove -q || echo_exit_chroot "$action - ERROR" ${LINENO}

    
action "Finaly distro-sync with no local rpm cache"
    rm -f /etc/yum.repos.d/local_-x86-64.repo
    dnf repoquery --refresh >/dev/null
    dnf distro-sync -y || echo_exit_chroot "$action - ERROR" ${LINENO}
    rpm -qa > /BUILD_DIR/${STAGE}_post.rpmqa
    mkdir -p /var/lib/rpm/modules/
    cp /BUILD_DIR/${STAGE}_post.rpmqa /var/lib/rpm/modules/00-basesys.trm.xzm

action "Save rpm cache then dnf clean all"
[ -d /BUILD_DIR/RPMCACHE ] &&  find /var/cache/dnf -name "*rpm" -type f |while read rpm ; do
		echo "to cache --> $rpm"
		mv -f "$rpm" /BUILD_DIR/RPMCACHE/ 
    done 
    dnf clean all
    rm -f /etc/yum.repos.d/betcher_-x86-64.repo

action "Install check"
	if ! [ -z "$(cat /BUILD_DIR/NOTINSTALLED)" ] ; then
		echo "These packages are not installed:"
		cat /BUILD_DIR/NOTINSTALLED
		echo_exit_chroot "$action - ERROR" ${LINENO}
	fi

