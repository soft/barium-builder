#!/bin/bash
. /chroot_patches/functions

action "Setting up dnf"
	setup-dnf || echo_exit_chroot "$action - ERROR" ${LINENO}

action "Check updates"
    if [ $(dnf check-update -q --refresh |wc -l) -ne 0 ]; then
		echo "Updates found"
		echo "You need to rebuild $PRESTAGE first"
		echo_exit_chroot "$action - ERROR" ${LINENO}
    fi

action "Install packages"
	> /BUILD_DIR/NOTINSTALLED
	#rpms=$(cat /chroot_patches/rootfs.add |xargs dnf repoquery --latest-limit 1 -q 2>/dev/null)
	bcfg_add=''
	[ -f /chroot_patches/${BUILDCFG}.add ] && bcfg_add=/chroot_patches/${BUILDCFG}.add
	cat /chroot_patches/rootfs.add $bcfg_add |while read a  ; do
		if echo $a |egrep -q '^[[:space:]]*\#' ; then
		    echo "Commented: $a"
		    continue
		fi
		echo "Processing: $a"
		dnf install "$a" -b -y || echo $a >> /BUILD_DIR/NOTINSTALLED
	done




action "DNF autoremove"
    dnf autoremove || echo_exit_chroot "$action - ERROR" ${LINENO}
	
action "Finaly distro-sync with no local rpm cache "
	if dnf upgrade ; then
		rm -f /etc/yum.repos.d/local_-x86-64.repo
		dnf repoquery --refresh >/dev/null 
		dnf distro-sync -y || echo_exit_chroot "$action - ERROR" ${LINENO}
		rpm -qa > /BUILD_DIR/${STAGE}.rpmqa
		cat /BUILD_DIR/${PRESTAGE}_post.rpmqa /BUILD_DIR/${PRESTAGE}_post.rpmqa /BUILD_DIR/${STAGE}.rpmqa |sort |uniq -u > /BUILD_DIR/${STAGE}_delta.rpmqa
		mkdir -p /var/lib/rpm/modules/
		cp /BUILD_DIR/${STAGE}_delta.rpmqa /var/lib/rpm/modules/10-extsys.xzm
	else
		echo_exit_chroot "$action - ERROR" ${LINENO}
	fi
   
action "Save rpm caches then dnf clean all"
[ -d /BUILD_DIR/RPMCACHE ] &&     find /var/cache/dnf -name "*rpm" -type f |while read rpm ; do
	mv -f "$rpm" /BUILD_DIR/RPMCACHE/  && echo "cache <-- $rpm"
    done 
    dnf clean all
    rm -f /etc/yum.repos.d/betcher_-x86-64.repo


action "Install check"
	if ! [ -z "$(cat /BUILD_DIR/NOTINSTALLED)" ] ; then
		echo "These packages are not installed:"
		cat /BUILD_DIR/NOTINSTALLED
		echo_exit_chroot "$action - ERROR" ${LINENO}
	fi

action "Check updates"
    if [ $(dnf check-update -q --refresh |wc -l) -ne 0 ]; then
		echo "Updates found"
		echo "You need to rebuild $STAGE again "
		echo_exit_chroot "$action - ERROR" ${LINENO}
    fi
