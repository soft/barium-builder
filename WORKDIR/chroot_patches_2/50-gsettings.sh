#!/bin/bash
exit 0
. /chroot_patches/functions

action "gsettings"
sed -i   's/>true</>false</'  /usr/share/glib-2.0/schemas/org.mate.media-handling.gschema.xml

glib-compile-schemas  /usr/share/glib-2.0/schemas/ || echo_exit_chroot "$action - ERROR" 1

