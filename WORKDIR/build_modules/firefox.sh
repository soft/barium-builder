#!/bin/bash
PACKS="firefox-ru"

if [ $(dnf check-update  -q --refresh |wc -l) -ne 0 ]; then
	echo "Updates found"
	dnf check-update
	echo "You need to rebuild $PRESTAGE first"
	read qqq
fi

if [ -d /BUILD_DIR/RPMCACHE ] ; then
dnf install createrepo_c 
createrepo_c /BUILD_DIR/RPMCACHE
[ $? == 0 ] && cat <<EOF > /etc/yum.repos.d/local_-x86-64.repo
[local-x86_64]
name=local-x86_64
baseurl=file:///BUILD_DIR/RPMCACHE
gpgcheck=0
enabled=1
EOF
fi

dnf install -b -y $PACKS

[ -d /BUILD_DIR/RPMCACHE ] && find /var/cache/dnf -name "*rpm" -type f |while read rpm ; do
	echo "to cache --> $rpm"
	mv -f "$rpm" /BUILD_DIR/RPMCACHE/ 
done 
dnf clean all
rm -rf /etc/yum.repos.d

# Зачищаем перед запаковкой модуля, нужно для пересборки в системе
# В сборочной отфильтруется и без этого
rm -rf /var/lib/rpm 		2>/dev/null
rm -rf /var/cache/dnf 		2>/dev/null
rmdir /* 			2>/dev/null
exit 0
