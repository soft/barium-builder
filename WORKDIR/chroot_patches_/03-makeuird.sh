#!/bin/bash
. /chroot_patches/functions
mkdir /BUILD_DIR 2>/dev/null
cd /BUILD_DIR

if [ -f ./uird/mkuird -a -f ./uird/busybox/busybox -a -L ./uird/dracut/dracut-install ] ; then  
    cd uird
    git pull origin master
    #git checkout testing
    #git pull origin testing:testing
else
	action "Download  UIRD and submodules"
	if ! git clone --recurse-submodules https://github.com/neobht/uird.git ; then
		rm -rf ./uird 
		# git clone валится с ошибками на разных стадиях загрузки гит реп, в основном на бизибоксе
		# по этому wget. Тоже падает, но с нескольких попыток догружает. Видимо с гитхабом что-то.
		wget -c -t 10 https://github.com/neobht/uird/archive/master.zip -O uird.zip || echo_exit_chroot "$action" 4
		wget -c -t 10 https://github.com/dracutdevs/dracut/archive/master.zip -O dracut.zip || echo_exit_chroot "$action" 4
		wget -c -t 10 https://github.com/mirror/busybox/archive/master.zip -O busybox.zip || echo_exit_chroot "$action" 4
		unzip uird.zip ; mv uird-master uird
		unzip dracut.zip ; mv -T ./dracut-master ./uird/dracut 
		unzip busybox.zip ; mv -T ./busybox-master ./uird/busybox
		rm  -f ./*.zip
	fi
	cd uird || echo_exit_chroot "$action" 4
fi

action "Find newest installed kernel"
NEWEST_KERNEL=$(ls -1 /lib/modules |grep -v 'flow-abi' |sort -n |tail -n1)
echo "Newest kernel is: $NEWEST_KERNEL"
cp /boot/vmlinuz-$NEWEST_KERNEL /BUILD_DIR/vmlinuz || echo_exit_chroot "Kernel not found" 4

action "Build UIRD"
[ -f ./busybox/busybox ] || ./make_busybox.sh 
[ -L ./dracut/dracut-install ] || ./make_dracut.sh

action "Build httpfs (magos-version)"
    pushd /chroot_patches/httpfs
	make
	mv httpfs /usr/bin/
    popd


cp /chroot_patches/Rosa* ./configs/uird_configs/
sed -i 's#SYSMNT.*#SYSMNT=/.memory#' ./configs/etc/initvars
# для установленного пакета mkuird
# cp /chroot_patches/Rosa* /usr/share/uird/configs/uird_configs/
# sed -i 's#SYSMNT.*#SYSMNT=/.memory#' /usr/share/uird/configs/etc/initvars

rm -f ./uird.Rosa.cpio.xz 2>/dev/null
./mkuird Rosa -k $NEWEST_KERNEL $(cat /chroot_patches/uird.cmdline)
cd ../
mv ./uird/uird.Rosa.cpio.xz /BUILD_DIR/
[ -f /BUILD_DIR/uird.Rosa.cpio.xz ] || echo_exit_chroot "$action" 5

