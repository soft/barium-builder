#!/bin/bash
. /chroot_patches/functions
PACKAGES="git-core kmod-devel gcc make wget mkuird grub2 grub2-efi syslinux-static aria2 lib64fuse2-devel curlftpfs grub2-theme-rosa e2fsprogs btrfs-progs"

action "Setting up dnf"
	setup-dnf || echo_exit_chroot "$action - ERROR" ${LINENO}

action "Install additional packages"
dnf install -y $PACKAGES || echo_exit_chroot "$action - ERROR" ${LINENO}

action "Save rpm cache"


[ -d /BUILD_DIR/RPMCACHE ] &&    find /var/cache/dnf -name "*rpm" -type f |while read rpm ; do
		echo "to cache --> $rpm"
		mv -f "$rpm" /BUILD_DIR/RPMCACHE/ 
    done 
    #dnf clean all
    #rm -f /etc/yum.repos.d/{betcher_-x86-64.repo,local_-x86-64.repo}


