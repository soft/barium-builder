#!/bin/bash
. /chroot_patches/functions
rm -rf /BUILD_DIR/grub2
mkdir -p  /BUILD_DIR/grub2 2>/dev/null

cd /BUILD_DIR/grub2

cat << EOF > ./grub.cfg 
search.file /boot/grub2/grub_efi.cfg root
prefix=(\$root)/boot/grub2
export root
export prefix
configfile \$prefix/grub.cfg
EOF
cat ./grub.cfg

grub2-mkimage -p /boot/grub2 -c ./grub.cfg -O i386-pc -o core.img --compression=xz  \
    biosdisk part_msdos part_gpt linux  loopback normal configfile \
    test search search_fs_uuid search_fs_file true iso9660 search_label \
    vbe gfxterm gfxmenu  fat ext2 ntfs cat echo ls

grub2-mkimage -p /boot/grub2 -c ./grub.cfg -O x86_64-efi -o grubx64.efi --compression=xz  \
    part_msdos part_gpt linux loopback normal configfile \
    test search search_fs_uuid search_fs_file true iso9660  search_label  \
    efi_gop efi_uga gfxterm gfxmenu fat ext2 ntfs cat echo ls 

#cat $0
#/bin/bash
[ -f ./core.img -a -f ./grubx64.efi ] || echo_exit_chroot "$action" ${LINENO}
cd /

