#!/bin/bash
. /chroot_patches/functions
mkdir /BUILD_DIR 2>/dev/null
cd /BUILD_DIR

action "Get pfs-utils"
if ! [ -f ./pfs-utils-cli/project-files/usr/bin/pfs ]  ; then  
	if ! git clone https://github.com/pfs-utils/pfs-utils-cli.git ; then
		wget -c -t 10  https://github.com/pfs-utils/pfs-utils-cli/archive/master.zip -O pfs-utils.zip
		unzip pfs-utils.zip && rm -f ./pfs-utils.zip
		mv ./pfs-utils-cli-master ./pfs-utils-cli
	fi
fi 
cd ./pfs-utils-cli
#git pull origin master
git checkout pfsget
git pull origin pfsget:pfsget
cd ../

[ -f ./pfs-utils-cli/project-files/usr/bin/pfs ] || echo_exit_chroot "$action" ${LINENO}




    
