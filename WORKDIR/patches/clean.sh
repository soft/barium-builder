#!/bin/bash
[ "$1" ] || exit
ROOT=$1

# проверка на rm -rf /*
[ $ROOT == "/" ] && exit

: > ${ROOT}/etc/fstab

# файлы в корне ( в т.ч. chroot_actions.sh )
rm -f ${ROOT}/* 2>/dev/null

#большие иконки
if [ -d ${ROOT}/usr/share/icons ] ; then 
    for a in `find ${ROOT}/usr/share/icons -type d | egrep -e '[x/]512$|[x/]256$' ` ;do  
	echo -n .
	rm -fr "$a" 
    done
fi
echo ""
exit 0
