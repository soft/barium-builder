#!/bin/bash
#echo "DISABLED" && exit 0
ROOT=$1

PFP=${ROOT}/lib/systemd/system/lightdm.service
if [ -f $PFP ] ; then 
cat > $PFP <<EOF
[Unit]
Description=Light Display Manager
Documentation=man:lightdm(1)
Conflicts=getty@tty1.service
After=systemd-user-sessions.service getty@tty1.service

[Service]
ExecStart=/usr/sbin/lightdm
Restart=always
IgnoreSIGPIPE=no
BusName=org.freedesktop.DisplayManager

[Install]
Alias=display-manager.service
EOF
fi

PFP=${ROOT}/lib/systemd/system/plymouth-quit.service
if [ -f $PFP ] ; then 
cat > $PFP <<EOF
[Unit]
Description=Terminate Plymouth Boot Screen
After=rc-local.service plymouth-start.service systemd-user-sessions.service  plymouth-quit-wait.service

[Service]
ExecStart=-/bin/plymouth quit
Type=oneshot
TimeoutSec=0
EOF
fi 


PFP=${ROOT}/lib/systemd/system/plymouth-quit-wait.service
if [ -f $PFP ] ; then 
cat > $PFP <<EOF
[Unit]
Description=Hold until boot process finishes up
After=rc-local.service plymouth-start.service systemd-user-session.service lightdm.service

[Service]
ExecStart=-/usr/lib/rosa-rw/sbin/plwait
Type=oneshot
TimeoutSec=30
EOF
fi


halt_services="/lib/systemd/system/plymouth-halt.service /lib/systemd/system/plymouth-reboot.service 
              /lib/systemd/system/plymouth-poweroff.service /lib/systemd/system/plymouth-kexec.service"
for service in $halt_services ; do
    PFP=${ROOT}$service 
    sed -i 's:^ExecStart=.*:ExecStart=/bin/true:' $PFP
    sed -i 's:^ExecStartPost=.*$::' $PFP
done

exit 0