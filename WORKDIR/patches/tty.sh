#!/bin/bash
echo "DISABLED"
exit 
ROOT=$1
sed -i s/.*NAutoVTs=.*/NAutoVTs=0/   ${ROOT}/etc/systemd/logind.conf
sed -i s/.*ReserveVT=.*/ReserveVT=0/ ${ROOT}/etc/systemd/logind.conf
exit 0