#!/bin/bash
ROOT=$1

FILE=${ROOT}/etc/os-release
cat >$FILE <<EOF
NAME="ROSA BARIUM"
VERSION="2021.1"
ID=rosa
VERSION_ID=2021.1
PRETTY_NAME="BARIUM"
ANSI_COLOR="1;34"
HOME_URL="https://www.rosalinux.ru/"
BUG_REPORT_URL="https://bugzilla.rosalinux.ru/"
EOF

FILE=${ROOT}/etc/oem
cat >$FILE <<EOF
SYSTEM="ROSA BARIUM"
PRODUCT=2021.1
EOF

ln -sf user.png /usr/share/faces/default.png

cat >${ROOT}/etc/xdg/user-dirs.defaults  <<EOF
# Default settings for user directories
#
# The values are relative pathnames from the home directory and
# will be translated on a per-path-element basis into the users locale
DESKTOP=Desktop
DOWNLOAD=Downloads
DOCUMENTS=Documents
MUSIC=Documents/Music
PICTURES=Documents/Pictures
VIDEOS=Documents/Videos
TEMPLATES=Documents/Templates
#PUBLICSHARE=Public
EOF

echo "ROSA BARIUM" > ${ROOT}/etc/redhat-release
exit 0
