#!/bin/bash
ROOT=$1
function hidemenuentry
{
 if [ -f ${ROOT}/usr/share/applications/$1.desktop ] ;then
    if ! grep -q "^NotShowIn=MATE" ${ROOT}/usr/share/applications/$1.desktop ;then
       if grep -q "^NotShowIn=" ${ROOT}/usr/share/applications/$1.desktop ;then
         sed -i s/^NotShowIn=/"NotShowIn=MATE;"/ ${ROOT}/usr/share/applications/$1.desktop
       else
         sed -i s/^Exec=/"NotShowIn=MATE\nExec="/ ${ROOT}/usr/share/applications/$1.desktop
       fi
    fi
 fi
}

SYSCONF=${ROOT}/etc/sysconfig
[ -d $SYSCONF ] || SYSCONF=${ROOT}/etc/ROSA-RW
ln -sf /lib/systemd/system/lightdm.service ${ROOT}/etc/systemd/system/display-manager.service
echo -e "DESKTOP=MATE\nDISPLAYMANAGER=lightdm" > $SYSCONF/desktop
sed -i s/Theme=.*/Theme=Rosa-EE/ ${ROOT}/etc/plymouth/plymouthd.conf
rm -f ${ROOT}/usr/share/plymouth/themes/default.plymouth
ln -sf Rosa-EE ${ROOT}/usr/share/plymouth/themes/default.plymouth

PFP=${ROOT}/usr/share/xsessions/mate.desktop
sed -i /"^\[ru\]"/d $PFP
sed -i s/"^Comment="/"Name[ru]=Рабочая станция (mate)\nComment="/ $PFP
sed -i s/"^Exec="/"Comment[ru]=Рабочая станция с доступом к документам и программам\nExec="/ $PFP

PFP=${ROOT}/usr/share/xsessions/xfce.desktop
sed -i /"^\[ru\]"/d $PFP
sed -i s/"^Comment="/"Name[ru]=Рабочая станция (xfce)\nComment="/ $PFP
sed -i s/"^Exec="/"Comment[ru]=Рабочая станция с доступом к документам и программам\nExec="/ $PFP

PFP=${ROOT}/usr/share/xsessions/i3term.desktop
cp -p ${ROOT}/usr/share/xsessions/i3.desktop $PFP
sed -i s/=i3$/=i3term/ $PFP
sed -i /"^\[ru\]"/d $PFP
sed -i s/"^Comment="/"Name[ru]=Терминал\nComment="/ $PFP
sed -i s/"^Exec="/"Comment[ru]=Ограниченный доступ пользователя только к удалённому терминалу\nExec="/ $PFP

rm -f ${ROOT}/etc/skel/.remmina/1289458408692.remmina ${ROOT}/usr/share/xsessions/i3.desktop ${ROOT}/usr/share/xsessions/i3-with-shmlog.desktop ${ROOT}/usr/share/applications/QMPlay2_enqueue.desktop  2>/dev/null

for a in pavucontrol rxvt-unicode scim-setup ;do
  hidemenuentry $a
done
exit 0
