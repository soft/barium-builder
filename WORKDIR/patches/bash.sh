#!/bin/bash
ROOT=$1
PFP=${ROOT}/etc/skel/.bash_profile
[ -f $PFP ] || exit 0
if ! grep -q "/usr/lib/rosa-rw/scripts" $PFP ;then
   sed -i s%^PATH=.*%'PATH=/usr/lib/rosa-rw/scripts:$PATH:$HOME/bin\nXDG_CONFIG_HOME=$HOME/.config'% $PFP
   sed -i s%^'export PATH'.*%'export XDG_CONFIG_HOME\nexport PATH'% $PFP
   echo ulimit -c 0 >> $PFP
fi
PFP=${ROOT}/root/.bashrc
if ! grep -q "/usr/lib/rosa-rw/scripts" $PFP ; then 
    echo "PATH=/usr/lib/rosa-rw/scripts:/sbin:/usr/sbin:/bin:/usr/bin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:$PATH" >> $PFP
    echo "export PATH" >> $PFP
    echo '' >> $PFP
fi
exit 0
