#!/bin/bash
# ---------------------------------------------------
# Script to create hibryd bootable iso
# author: Betkher A. <a.betkher@rosalinux.ru>
# ---------------------------------------------------
if [ "$1" = "--help" -o "$1" = "-h" ]; then
  t_echo "This script will create bootable ISO"
  exit
fi

CDLABEL=BARIUM
[ $1 ] && CDLABEL="$1"

rm -f efi.img rosa.img 
dd if=/dev/zero of=./efi.img bs=1 count=0 seek=100M
mkfs.vfat ./efi.img
mkdir ./efiimg
mount ./efi.img ./efiimg
cp ./EFI ./efiimg/ -fr
cp ./boot ./efiimg/ -fr
umount ./efiimg/
rmdir ./efiimg/

dd if=/dev/zero of=./rosa.img bs=1 count=0 seek=100M
mkfs.ext4 ./rosa.img
mkdir ./rosaimg
mount ./rosa.img ./rosaimg
cp ./ROSA-DATA ./rosaimg/ -fax
: > ./rosaimg/EXPAND_ME
umount ./rosaimg/
rmdir ./rosaimg/

SUGGEST=$(readlink -f ./$CDLABEL.iso)
[ "$ISONAME" = "" ] && ISONAME="$SUGGEST" 
xorriso_opt="--no-emul-boot \
-b isolinux.bin \
-iso_mbr_part_type 0x83 \
-no-emul-boot -boot-load-size 4 \
-boot-info-table --partition_offset 16 \
--boot-catalog-hide \
-isohybrid-mbr boot/syslinux/isohdpfx.bin \
--mbr-force-bootable \
-eltorito-alt-boot \
-e efi.img \
--no-emul-boot \
-append_partition 2 0xef ./efi.img  \
-append_partition 3 0x83 ./rosa.img  \
--graft-points boot=boot EFI=EFI ROSA-SYSTEM=ROSA-SYSTEM \
isomode=boot/syslinux/isolinux.cfg \
isolinux.bin=boot/syslinux/isolinux.bin \
efi.img=efi.img"

xorrisofs -o "$ISONAME" -v -J -R -D -A "$CDLABEL" -V "$CDLABEL" $xorriso_opt
rm -f efi.img
rm -f rosa.img

